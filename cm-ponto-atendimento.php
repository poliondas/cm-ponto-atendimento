<?php

/**
 * @package CmPontoAtendimento
 */
/*
  Plugin Name: Cm Ponto Atendimento
  Depends: .
  Plugin URI: https://bitbucket.org/poliondas/cm-ponto-atendimento/overview
  Description: Cria pontos de atendimento do tipo loja ou representante.
  Version: 1.0.0
  Author: Carlos Eduardo Tormina Mateus
  Author URI: https://www.linkedin.com/in/carlos-mateus-52605a9a/
  License: GPLv2 or later
  Text Domain: site
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('CMPONTOATENDIMENTO_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('CMPONTOATENDIMENTO_PLUGIN_URL', WP_PLUGIN_URL . '/cm-ponto-atendimento');
define('CMPONTOATENDIMENTO_PLUGIN_URI', CMPONTOATENDIMENTO_PLUGIN_URL);

register_activation_hook(__FILE__, array('CmPontoAtendimento', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('CmPontoAtendimento', 'plugin_deactivation'));

require_once( CMPONTOATENDIMENTO_PLUGIN_DIR . 'post-type-ponto-atendimento.php' );
require_once( CMPONTOATENDIMENTO_PLUGIN_DIR . 'class.cmpontoatendimento.php' );

add_action('init', array('CmPontoAtendimento', 'init'));
add_action('wp_enqueue_scripts', array('CmPontoAtendimento', 'add_script_custom'));
add_action('admin_enqueue_scripts', array('CmPontoAtendimento', 'add_admin_script_custom'));
