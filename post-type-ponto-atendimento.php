<?php

class PostTypePontoAtendimento
{

    protected $post_type = 'cm_pa';
    protected $labels = array('CM Ponto Atendimento', 'CM Ponto Atendimento', 'CM Ponto Atendimento', 'Categorias');
    protected $metabox = array(
        "cm_pa_tipo" => 'select',
        "cm_pa_cep" => 'text',
    );

    public function __construct()
    {
        //- Hook into the 'init' action
        add_action('init', array($this, 'register'), 0);
        // add_action( 'init', array($this, 'custom_taxonomy'), 0 );
        // add_action('pre_get_posts', array($this, 'search_filter'));
        //- Cria os metabox
        add_filter('rwmb_meta_boxes', array($this, 'registerMetaBox'));

        //- Adiciona js e css
//          add_action('wp_enqueue_scripts', array($this, 'add_script_custom'));
    }

    // Register Custom Post Type
    public function register()
    {
        $labels = array(
            'name' => 'CM Ponto de Atendimento',
            'singular_name' => 'CM Ponto de Atendimento',
            'menu_name' => 'CM Ponto de Atendimento',
            'name_admin_bar' => 'CM Ponto de Atendimento',
            'archives' => 'CM Ponto de Atendimento Archives',
            'attributes' => 'CM Ponto de Atendimento Attributes',
            'parent_item_colon' => 'Item Pai:',
            'all_items' => 'Todos os pontos de atendimento',
            'add_new_item' => 'Adicionar novo CM Ponto de Atendimento',
            'add_new' => 'Adicionar novo',
            'new_item' => 'Novo CM Ponto de Atendimento',
            'edit_item' => 'Editar CM Ponto de Atendimento',
            'update_item' => 'Atualizar CM Ponto de Atendimento',
            'view_item' => 'Visualizar CM Ponto de Atendimento',
            'view_items' => 'Visualizar CM Ponto de Atendimento',
            'search_items' => 'Buscar CM Ponto de Atendimento',
            'not_found' => 'Não encontrado',
            'not_found_in_trash' => 'Não encontrado na lixeira',
            'featured_image' => 'Imagem destacada',
            'set_featured_image' => 'Definir imagem destacada',
            'remove_featured_image' => 'Remover imagem destacada',
            'use_featured_image' => 'Usar como imagem destacada',
            'insert_into_item' => 'Inserir no item',
            'uploaded_to_this_item' => 'Uploaded para este item',
            'items_list' => 'Lista de itens',
            'items_list_navigation' => 'Navegação da lista de itens',
            'filter_items_list' => 'Filtrar lista de itens',
        );
        $args = array(
            'label' => 'CM Ponto de Atendimento',
            'description' => 'CM Ponto de Atendimento',
            'labels' => $labels,
            'supports' => array('title', 'editor', 'thumbnail'),
            'taxonomies' => array(),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 20,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'capability_type' => 'page',
            'menu_icon' => 'dashicons-location-alt',
        );

        $this->custom_taxonomy();
        register_post_type('cm_pa', $args);
    }

    public function search_filter($query)
    {
        if (!is_admin() && $query->is_main_query()) {
            if (is_tax('mk_especialidades')) {
                $query->set('post_type', array('cm_pa'));
            }
        }
    }

    public function custom_taxonomy()
    {
        // Register Custom Taxonomy
        $labels = array(
            'name' => _x('Classsificações', 'Taxonomy General Name', 'text_domain'),
            'singular_name' => _x('Classsificação', 'Taxonomy Singular Name', 'text_domain'),
            'menu_name' => __('Classsificações', 'text_domain'),
            'all_items' => __('All Items', 'text_domain'),
            'parent_item' => __('Parent Item', 'text_domain'),
            'parent_item_colon' => __('Parent Item:', 'text_domain'),
            'new_item_name' => __('New Item Name', 'text_domain'),
            'add_new_item' => __('Add New Item', 'text_domain'),
            'edit_item' => __('Edit Item', 'text_domain'),
            'update_item' => __('Update Item', 'text_domain'),
            'view_item' => __('View Item', 'text_domain'),
            'separate_items_with_commas' => __('Separate items with commas', 'text_domain'),
            'add_or_remove_items' => __('Add or remove items', 'text_domain'),
            'choose_from_most_used' => __('Choose from the most used', 'text_domain'),
            'popular_items' => __('Popular Items', 'text_domain'),
            'search_items' => __('Search Items', 'text_domain'),
            'not_found' => __('Not Found', 'text_domain'),
            'no_terms' => __('No items', 'text_domain'),
            'items_list' => __('Items list', 'text_domain'),
            'items_list_navigation' => __('Items list navigation', 'text_domain'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        );
        register_taxonomy('cm_pa_classificacao', array('cm_pa'), $args);
    }

    /**
     * Register Metabox
     */
    public function registerMetaBox($meta_boxes)
    {
        $prefix = 'cm_pa_';
        #Geral
        $meta_boxes[] = array(
            'title' => 'Geral',
            'priority' => 'high',
            'post_types' => array($this->post_type),
            'fields' => array(
                #SELECT
                array(
                    'name' => 'Tipo',
                    'id' => "{$prefix}tipo",
                    'type' => 'select',
                    'desc' => 'Selecione um tipo de ponto de atendimento',
                    // Array of 'value' => 'Label' pairs
                    'options' => array(
                        'l' => 'Loja',
                        'r' => 'Representante',
                    ),
                ),
            )
        );
        #Endereço
        $meta_boxes[] = array(
            'title' => 'Endereço',
            'priority' => 'high',
            'post_types' => array($this->post_type),
            'fields' => array(
                #TEXT
                array(
                    'name' => 'CEP',
                    'id' => "{$prefix}cep",
                    'desc' => 'CEP da loja ou do endereço principal do representante (Apenas números)',
                    'type' => 'text',
                ),
                #TEXT
                array(
                    'name' => 'Cidade',
                    'id' => "{$prefix}cidade",
                    'desc' => 'Preenchido automaticamente ao informar o CEP',
                    'type' => 'text',
                    'readonly' => 1,
                ),
                #TEXT
                array(
                    'name' => 'UF',
                    'id' => "{$prefix}uf",
                    'desc' => 'Preenchido automaticamente ao informar o CEP',
                    'type' => 'text',
                    'readonly' => 1,
                ),
                #TEXT
                array(
                    'name' => 'Nº',
                    'id' => "{$prefix}numero",
                    'type' => 'text',
                ),
            ),
        );
        #Contato
        $meta_boxes[] = array(
            'title' => 'Contato',
            'priority' => 'high',
            'post_types' => array($this->post_type),
            'fields' => array(
                #TEXT
                array(
                    'name' => 'Fone 1',
                    'id' => "{$prefix}fone_1",
                    'type' => 'text',
                ),
                #TEXT
                array(
                    'name' => 'Fone 2',
                    'id' => "{$prefix}fone_2",
                    'type' => 'text',
                ),
                #TEXT
                array(
                    'name' => 'E-mail',
                    'id' => "{$prefix}email",
                    'type' => 'text',
                ),
            )
        );
        /*
          #Background
          $meta_boxes[] = array(
          'title' => 'Imagem de fundo',
          'priority' => 'high',
          'post_types' => array($this->post_type),
          'fields' => array(
          // FILE ADVANCED (WP 3.5+)
          array(
          'name' => 'Image padrão, formato paisagem',
          'id' => "{$prefix}foto",
          'type' => 'file_advanced',
          'max_file_uploads' => 1,
          'mime_type' => 'image', // Leave blank for all file types
          'desc' => 'usada quando não há uma foto especificada para o tamanho',
          ),
          array(
          'type' => 'heading',
          'name' => 'Imagens específicas',
          'desc' => 'Use os campos abaixo para especificar fotos diferentes para tamanhos de tela diferentes',
          ),
          // FILE ADVANCED (WP 3.5+)
          array(
          'name' => 'Tela grande, formato paisagem',
          'id' => "{$prefix}foto_l_lg",
          'type' => 'file_advanced',
          'max_file_uploads' => 1,
          'mime_type' => 'image', // Leave blank for all file types
          ),
          // FILE ADVANCED (WP 3.5+)
          array(
          'name' => '<i class="fas fa-tablet"></i>Tela grande, formato retrato ',
          'id' => "{$prefix}foto_p_lg",
          'type' => 'file_advanced',
          'max_file_uploads' => 1,
          'mime_type' => 'image', // Leave blank for all file types
          ),
          // FILE ADVANCED (WP 3.5+)
          array(
          'name' => 'Tela pequena, formato paisagem',
          'id' => "{$prefix}foto_l_xs",
          'type' => 'file_advanced',
          'max_file_uploads' => 1,
          'mime_type' => 'image', // Leave blank for all file types
          ),
          // FILE ADVANCED (WP 3.5+)
          array(
          'name' => 'Tela pequena, formato retrato',
          'id' => "{$prefix}foto_p_xs",
          'type' => 'file_advanced',
          'max_file_uploads' => 1,
          'mime_type' => 'image', // Leave blank for all file types
          ),
          ),
          );
          $meta_boxes[] = array(
          'title' => 'Item',
          'priority' => 'high',
          'post_types' => array($this->post_type),
          'fields' => array(
          // FILE ADVANCED (WP 3.5+)
          array(
          'name' => 'Imagem',
          'id' => "{$prefix}item_image",
          'type' => 'file_advanced',
          'max_file_uploads' => 1,
          'mime_type' => 'image', // Leave blank for all file types
          ),
          // TEXT
          array(
          'name' => 'Classe para a imagem',
          'id' => "{$prefix}item_image_class",
          'desc' => 'classe CSS aplicada a image',
          'type' => 'text',
          ),
          // TEXTAREA
          array(
          'name' => 'Titulo',
          'desc' => 'cada linha será separada em um elemento &lt;span class="line-#"&gt;...&lt;/span&gt;',
          'id' => "{$prefix}item_titulo",
          'type' => 'textarea',
          'cols' => 20,
          'rows' => 3,
          ),
          // TEXT
          array(
          'name' => 'Classe para o título',
          'desc' => 'classe CSS aplicada ao título',
          'id' => "{$prefix}item_titulo_class",
          'type' => 'text',
          ),
          // TEXTAREA
          array(
          'name' => 'Texto',
          'desc' => 'texto simples',
          'id' => "{$prefix}item_texto",
          'type' => 'textarea',
          'cols' => 20,
          'rows' => 3,
          ),
          // TEXT
          array(
          'name' => 'Classe para o texto',
          'desc' => 'classe CSS aplicada ao texto',
          'id' => "{$prefix}item_texto_class",
          'type' => 'text',
          'std' => "",
          ),
          // TEXT
          array(
          'name' => 'Texto para o botão',
          'desc' => 'deixe em branco para não usar o link',
          'id' => "{$prefix}item_btn_texto",
          'type' => 'text',
          ),
          // TEXT
          array(
          'name' => 'Link para o botão',
          'id' => "{$prefix}item_btn_link",
          'type' => 'text',
          ),
          // TEXT
          array(
          'name' => 'Classe para o botão',
          'id' => "{$prefix}item_btn_class",
          'desc' => 'classe CSS aplicada ao botão',
          'type' => 'text',
          ),
          array(
          'type' => 'heading',
          'name' => 'Posicionamento do item dentro do banner',
          'desc' => 'Use os campos abaixo para especificar posições diferentes para tamanhos de tela diferentes',
          ),
          array(
          'name' => 'Posição padrão',
          'id' => "{$prefix}item_posicao",
          'type' => 'select',
          'placeholder' => 'Selecione uma posição',
          // Array of 'value' => 'Label' pairs
          'options' => array(
          'top-left' => 'Superior-Esquerda',
          'top-center' => 'Superior-Centro',
          'top-right' => 'Superior-Direita',
          'middle-left' => 'Centro-Esquerda',
          'middle-center' => 'Centro-Centro',
          'middle-right' => 'Centro-Direita',
          'bottom-left' => 'Inferior-Esquerda',
          'bottom-center' => 'Inferior-Centro',
          'bottom-right' => 'Inferior-Direita',
          ),
          ),
          array(
          'name' => 'Tela grande, formato paisagem',
          'id' => "{$prefix}item_posicao_l_lg",
          'type' => 'select',
          'placeholder' => 'Selecione uma posição',
          // Array of 'value' => 'Label' pairs
          'options' => array(
          'top-left' => 'Superior-Esquerda',
          'top-center' => 'Superior-Centro',
          'top-right' => 'Superior-Direita',
          'middle-left' => 'Centro-Esquerda',
          'middle-center' => 'Centro-Centro',
          'middle-right' => 'Centro-Direita',
          'bottom-left' => 'Inferior-Esquerda',
          'bottom-center' => 'Inferior-Centro',
          'bottom-right' => 'Inferior-Direita',
          ),
          ),
          array(
          'name' => 'Tela grande, formato retrato',
          'id' => "{$prefix}item_posicao_p_lg",
          'type' => 'select',
          'placeholder' => 'Selecione uma posição',
          // Array of 'value' => 'Label' pairs
          'options' => array(
          'top-left' => 'Superior-Esquerda',
          'top-center' => 'Superior-Centro',
          'top-right' => 'Superior-Direita',
          'middle-left' => 'Centro-Esquerda',
          'middle-center' => 'Centro-Centro',
          'middle-right' => 'Centro-Direita',
          'bottom-left' => 'Inferior-Esquerda',
          'bottom-center' => 'Inferior-Centro',
          'bottom-right' => 'Inferior-Direita',
          ),
          ),
          array(
          'name' => 'Tela pequena, formato paisagem',
          'id' => "{$prefix}item_posicao_l_xs",
          'type' => 'select',
          'placeholder' => 'Selecione uma posição',
          // Array of 'value' => 'Label' pairs
          'options' => array(
          'top-left' => 'Superior-Esquerda',
          'top-center' => 'Superior-Centro',
          'top-right' => 'Superior-Direita',
          'middle-left' => 'Centro-Esquerda',
          'middle-center' => 'Centro-Centro',
          'middle-right' => 'Centro-Direita',
          'bottom-left' => 'Inferior-Esquerda',
          'bottom-center' => 'Inferior-Centro',
          'bottom-right' => 'Inferior-Direita',
          ),
          ),
          array(
          'name' => 'Tela pequena, formato retrato',
          'id' => "{$prefix}item_posicao_p_xs",
          'type' => 'select',
          'placeholder' => 'Selecione uma posição',
          // Array of 'value' => 'Label' pairs
          'options' => array(
          'top-left' => 'Superior-Esquerda',
          'top-center' => 'Superior-Centro',
          'top-right' => 'Superior-Direita',
          'middle-left' => 'Centro-Esquerda',
          'middle-center' => 'Centro-Centro',
          'middle-right' => 'Centro-Direita',
          'bottom-left' => 'Inferior-Esquerda',
          'bottom-center' => 'Inferior-Centro',
          'bottom-right' => 'Inferior-Direita',
          ),
          ),
          ),
          );
         */
        #pages
        //$this->addMetaBoxPage($meta_boxes);

        return $meta_boxes;
    }

    protected function addMetaBoxPage(&$meta_boxes)
    {
        /*
          if (isset($_GET['post'])) {
          $post_id = $_GET['post'];
          } elseif (isset($_POST['post_ID'])) {
          $post_id = $_POST['post_ID'];
          }
          if (!isset($post_id)) {
          return;
          }
          //$templates = wp_get_theme()->get_page_templates();
          $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
          $aPageId = array('2' => 1);
          $aPageTemplate = array('pagina-inicial.php' => 1);

          if (isset($aPageId[$post_id]) || isset($aPageTemplate[$template_file])) {

          $prefix = 'cm_pa_';
          $aPontos de atendimento = $this->find();

          $options = array();
          foreach ($aPontos de atendimento as $banner) {
          $options[$banner['id']] = $banner['post_title'];
          }
          $meta_boxes[] = array(
          'title' => 'Pontos de atendimento',
          'priority' => 'high',
          'post_types' => array('page'),
          'fields' => array(
          // CHECKBOX
          array(
          'name' => 'Usar PA',
          'id' => "{$prefix}page_use",
          'type' => 'checkbox',
          ),
          array(
          'name' => 'PA',
          'id' => "{$prefix}page",
          'type' => 'select_advanced',
          //'type'    => 'select',
          'multiple' => true,
          'rows' => 5,
          'options' => $options,
          ),
          ),
          );
          }
         */
    }

    protected function getArgs($filter = array())
    {

        //- Argumento inciais
        $args = array(
            'post_type' => $this->post_type,
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'posts_per_page' => -1,
            'order' => 'ASC',
        );

        /*
          //- standard taxonomy
          if (!empty($filter['taxonomy'])) {
          $args['classificacao_de_' . $this->post_type] = $filter['taxonomy'];
          unset($filter['taxonomy']);
          }

          if (!empty($filter['paged']) && !isset($filter['posts_per_page'])) {
          $args['posts_per_page'] = get_option('posts_per_page');
          }
         */

        foreach ($filter as $key => &$val) {

            $args[$key] = $val;
        }


        return $args;
    }

    function find($filter = array())
    {
        global $post;

        $args = $this->getArgs($filter);

        $the_query = new WP_Query($args);

        wp_reset_postdata();

        return $this->createList($the_query);
    }

    protected function createList($result)
    {

        global $post;

        $aList = array();

        if ($result->have_posts()) {

            while ($result->have_posts()) {
                $result->the_post();

                $item = array(
                    'id' => $post->ID,
                    'post_date' => $post->post_date,
                    'post_title' => $post->post_title,
                    'post_name' => $post->post_name,
                    'post_content' => $post->post_content,
                    'post_excerpt' => $post->post_excerpt,
                    'comment_count' => $post->comment_count,
                );

                $this->addAditionalData($item);

                $aList[] = $item;
            }
        }

        return $aList;
    }

    protected function addAditionalData(&$item)
    {

        global $post;

        if (post_type_supports($this->post_type, 'thumbnail')) {
            $thumb_id = get_post_thumbnail_id();
            $thumb_url = ( $thumb_id ) ? wp_get_attachment_url($thumb_id) : '';
            $item['featured_image'] = $thumb_url;
        }

        foreach ($this->metabox as $key => $type) {

            $item[$key] = rwmb_meta($key, array('type' => $type, 'size' => 'medium'));
        }

        return $item;
    }

    public function add_script_custom()
    {
        //wp_enqueue_style('banner-style', CMPONTOATENDIMENTO_PLUGIN_URL . '/css/front.css', array(), null, 'all');
        //wp_enqueue_script('cm-cycle', CMPONTOATENDIMENTO_PLUGIN_URL . "/lib/cm-cycle2.js", array('main'), null, true);
        //wp_enqueue_script('cm-ponto-atendimento', CMPONTOATENDIMENTO_PLUGIN_URL . "/js/front.js", array('cm-cycle'), null, true);
    }

}

//- Instancia o PostType
global $postTypePA;
$postTypePA = new PostTypePontoAtendimento();
