<?php

class CmPontoAtendimento
{

    public static function init()
    {
        # code ...
        add_action('admin_init', array('CmPontoAtendimento', 'check_plugin_dependences'));
        add_action('wp_ajax_get_pa_near_cep', array('CmPontoAtendimento', 'get_pa_near_cep'));
        add_action('wp_ajax_nopriv_get_pa_near_cep', array('CmPontoAtendimento', 'get_pa_near_cep'));
    }

    public static function plugin_activation()
    {
        # code...
    }

    public static function plugin_deactivation()
    {
        # code ...
    }

    public static function check_plugin_dependences()
    {
        # code ...
    }

    public static function desactivate_plugin()
    {
        # code ...
    }

    public static function add_script_custom()
    {
        //wp_enqueue_script('ajax-notification', WP_PLUGIN_URL . '/cm-ponto-atendimento/js/admin.js', array('jquery'), null, true);
    }

    public static function add_admin_script_custom()
    {
        wp_enqueue_script('pontoatendimento', CMPONTOATENDIMENTO_PLUGIN_URL . '/js/back.js', array('jquery'), null, true);
        wp_enqueue_style('pontoatendimento', CMPONTOATENDIMENTO_PLUGIN_URL . '/css/back.css', array(), null, 'all');
    }

    /**
     * Consulta no banco todos os pontos de atendimento mais próximos a um cep
     *
     * @param string $cep
     * @param int $limit
     * @return array
     */
    public static function get_pa_near_cep($options = array())
    {
        $filtros = self::get_pa_near_cep_filtros($options);
        $sql = self::get_pa_near_cep_sql($filtros);
        global $wpdb;

        $rows = $wpdb->get_results($sql);
        $aPa = array();
        foreach ($rows as $row) {
            $aPa[] = $row;
        }
        if (isset($options['return']) && (boolean) $options['return'] === true) {
            return $aPa;
        } else {
            echo json_encode($aPa);
            wp_die();
        }
    }

    private static function get_pa_near_cep_filtros($options)
    {
        $limit = filter_input(INPUT_POST, 'limit');
        if ($limit === null) {
            $limit = isset($options['limit']) ? $options['limit'] : 5;
        }
        $cep = filter_input(INPUT_POST, 'cep');
        if ($cep === null) {
            $cep = isset($options['cep']) ? $options['cep'] : '00000000';
        }
        $tipo = filter_input(INPUT_POST, 'tipo');
        if ($tipo === null) {
            $tipo = isset($options['tipo']) ? $options['tipo'] : null;
        }
        $filtros = array(
            'cep' => $cep,
            'limit' => $limit,
        );
        if (!empty($tipo)) {
            $filtros['tipo'] = substr($tipo, 0, 1);
        }
        return $filtros;
    }

    private static function get_pa_near_cep_sql($filtros)
    {
        $cep = $filtros['cep'];
        $limit = $filtros['limit'];
        $sql = "
            SELECT p.*,
                ABS(CAST(pm.meta_value AS SIGNED) - {$cep}) AS cep_dif
            FROM wp_posts AS p
            INNER JOIN wp_postmeta AS pm ON pm.post_id = p.ID AND pm.meta_key = 'cm_pa_cep'
        ";

        if (isset($filtros['tipo'])) {

            $sql .= "INNER JOIN wp_postmeta AS pm2 ON pm2.post_id = p.ID AND pm2.meta_key = 'cm_pa_tipo'";
        }
        $sql .= "WHERE p.post_type = 'cm_pa'";
        if (isset($filtros['tipo'])) {
            $sql .= " AND pm2.meta_value = '{$filtros['tipo']}'";
        }
        $sql .= "ORDER BY cep_dif LIMIT 0,{$limit}";

        return $sql;
    }

}
