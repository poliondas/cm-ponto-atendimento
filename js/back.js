/* global ajaxurl, postId */
jQuery(function ($) {
    var $cmCep = $("#cm_pa_cep");
    var $cmCidade = $("#cm_pa_cidade");
    var $cmUf = $("#cm_pa_uf");
    
    $cmCep.data('cep', $cmCep.val());
    $cmCep.on('keyup', function(){
        if(this.value.length === 8){
            if($cmCep.data('cep') !== this.value || $cmCidade.val() === ''){
                $cmCep.data('cep', this.value);
                get_address_cep(this.value);
            }
        }
    });
    
    function get_address_cep(cep){
        $cmCidade.parent().addClass('loading');
        $cmUf.parent().addClass('loading');
        jQuery.ajax({
            url: "https://viacep.com.br/ws/" + cep +"/json/",
            type: 'get',
            success: function(data){
                $cmCidade.parent().removeClass('loading');
                $cmUf.parent().removeClass('loading');
                if(!data || data.erro){
                    alert('Erro: CEP não encontrado');
                    return;
                }
                $cmCidade.val(data.localidade);
                $cmUf.val(data.uf);
            }
        });
    }
    function get_pa_near_cep(cep){
        var fd = new FormData();
        fd.append('action', 'get_pa_near_cep');
        fd.append('cep', cep);
        
        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            contentType: false,
            processData: false,
            data: fd,
            success: function(msg){
                var data = JSON.parse(msg);
                console.log(data);
            }
        });
    }
});